# SprigBootApp



## Popis

Jedna sa o jednoduchu aplikaciu vytvorenu pomocou javy a frameworku springboot ktora vytvara,upravuje,maze uzivatelov.

##
Pre spustenie su dostupne 2 moznosti, jedna kde je pripojena PostgreSQL databaza a druha kde sa vyuzivva H2 in memory\
databaza ktora si nedokaze ulozit data medzi spusteniami.

Pre verziu s postgreSQL staci v subore aplication.properities nastavit pristupove udaje.
```
spring.datasource.url = jdbc:postgresql://localhost:5432/springbootapp
spring.datasource.username = admin
spring.datasource.password = admin
```
a vytvorenie jednoduchej tabulky ktoru aplikacia  pouziva pomocou nasledovneho SQL:

```
CREATE TABLE users (
	id integer PRIMARY KEY,
	name VARCHAR(40) NOT NULL,
	username VARCHAR(20) UNIQUE NOT NULL,
	password TEXT NOT NULL
)
```
\
Pre druhu verziu s H2 in memory databazou staci vyuzit  *aplication-h2.properities* properities suboru pri spustani.
## jednotlive dostupne endpointy

jednotlive endpointy sa daju dotazovat napriklad pomocou palikacie *postman*, ktoru som pouzil aj ja.
### POST user
**Dostupnost:** pre kazdeho

vytvori noveho uzivatela do databaze

priklad pouzitia:\
REST: POST http://localhost:8080/user
```
{
    "name": "Martin Kral",
    "username": "king",
    "password": "martinko"
}
```


### GET user/{id}
**Dostupnost:** pre kazdeho

Ak uzivatel s danym ID existuje tak vrati info o nom vo formate JSON

priklad pouzitia:\
REST: GET http://localhost:8080/user/1

### GET user/all
**Dostupnost:** pre kazdeho prihlaseneho uzivatela

Vo formate JSON vrati informacie o vsetkych uzivateloch\
Pouziva strankovanie takze je obmedzeny pocet uzivatelov ktorych zobrazi na 10,\
na dalsie stranky sa prepina pomocou parametru *pageNumber*

priklad pouzitia:\
REST: GET http://localhost:8080/user/all?pageNumber=0


### PATCH user
**Dostupnost:** Kazdy prihlaseny uzivatel si moze zmenit svoje udaje

Uzivatel posle vo  formate JSON zmeny ktore chce vykonat. Moze si zmenim *name*, *username*, *password*

priklad pouzitia:\
REST: PATCH http://localhost:8080/user
```
{
    "name": "Martin Kral Kralovsky",
    "username": "king",
    "password": "martinkoJeSuper"
}
```

### DELETE deleteUser/{id}
**Dostupnost:** prihlaseny uzivatel 

kazdy uzivatel moze zmazat svoj zaznam z databaze

priklad pouzitia:\
REST: http://localhost:8080/deleteUser/1

## Mozne vylepsenia

- chybaju testy, pridat aspon unit testy
- vylepsenie chybovych hlasok a exception handling aby bolo jasnejsie co sa nepodarilo pri neuspesnej operacii
- pridanie roli uzivatelom a nasledne pridanie a upravenie funkcionality jednotlivych endpointov
- pridanie frontendu
- docker, CI/CD pipelines, atd.
