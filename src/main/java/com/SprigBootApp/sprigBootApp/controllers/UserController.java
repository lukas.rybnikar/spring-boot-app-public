package com.SprigBootApp.sprigBootApp.controllers;

import com.SprigBootApp.sprigBootApp.exceptions.UserException;
import com.SprigBootApp.sprigBootApp.model.dto.CreateOrUpdateUserDto;
import com.SprigBootApp.sprigBootApp.model.dto.UserDto;
import com.SprigBootApp.sprigBootApp.model.entity.User;
import com.SprigBootApp.sprigBootApp.service.UserService;
import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static com.SprigBootApp.sprigBootApp.utils.AuthorizationUtil.getAuthorizedUser;

@RestController
@RequestMapping("/user")
public class UserController {


    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @PostMapping("")
    public ResponseEntity<UserDto> addUser(@RequestBody @Valid CreateOrUpdateUserDto userDto){
        try {
            User user = userService.createUser(userDto.getUsername(), userDto.getPassword(), userDto.getName());
            return ResponseEntity.ok(new UserDto(user));
        }catch (UserException ex){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable int id) {
        Optional<User> user = userService.getUser(id);
        if (user.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return ResponseEntity.ok(new UserDto(user.get()));
    }

    @GetMapping("/all")
    public ResponseEntity<Page<UserDto>> getAllUsers(@RequestParam int pageNumber) {
        PageRequest pageable = PageRequest.of(pageNumber, 10);
        Page<User> usersPage =  userService.getAllUsers(pageable);
        List<UserDto> userDtoList = usersPage.stream()
                .map(UserDto::new)
                .toList();
        return ResponseEntity.ok(new PageImpl<>(userDtoList,pageable, usersPage.getTotalElements()));
    }


    @PatchMapping("")
    public ResponseEntity<UserDto> updateUser(@RequestBody @Valid CreateOrUpdateUserDto userDto) {
        Optional<User> authorizedUser = getAuthorizedUser();
        if (authorizedUser.isPresent()) {
            try {
                User user = userService.updateUser(authorizedUser.get().getId(), userDto.getUsername(), userDto.getPassword(), userDto.getName());
                return ResponseEntity.ok(new UserDto(user));
            } catch (UserException ex){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

    }
}
