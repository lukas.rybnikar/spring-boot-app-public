package com.SprigBootApp.sprigBootApp.controllers;

import com.SprigBootApp.sprigBootApp.model.entity.User;
import com.SprigBootApp.sprigBootApp.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static com.SprigBootApp.sprigBootApp.utils.AuthorizationUtil.getAuthorizedUser;

@RestController
@RequestMapping("/deleteUser")
public class DeleteUserController {

    private final UserService userService;


    public DeleteUserController(UserService userService) {
        this.userService = userService;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUserById(@PathVariable int id) {
        Optional<User> authorizedUser = getAuthorizedUser();
        if (authorizedUser.isPresent()) {
            if (authorizedUser.get().getId() == id) {
                userService.deleteUserById(id);
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
