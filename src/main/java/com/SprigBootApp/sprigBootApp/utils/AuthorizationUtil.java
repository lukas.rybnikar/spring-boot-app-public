package com.SprigBootApp.sprigBootApp.utils;

import com.SprigBootApp.sprigBootApp.model.CustomUserDetails;
import com.SprigBootApp.sprigBootApp.model.entity.User;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class AuthorizationUtil {

    private AuthorizationUtil(){};

    public static Optional<User> getAuthorizedUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof CustomUserDetails) {
            return Optional.of(((CustomUserDetails)principal).getUserAccount());
        } else {
            return Optional.empty();
        }
    }


}
