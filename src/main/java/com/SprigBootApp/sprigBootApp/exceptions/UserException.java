package com.SprigBootApp.sprigBootApp.exceptions;

public class UserException extends Exception{
    private final UserExceptionCode code;

    public UserException(UserExceptionCode code) {
        super(code.name());
        this.code = code;
    }
}
