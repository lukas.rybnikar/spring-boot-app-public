package com.SprigBootApp.sprigBootApp.service;

import com.SprigBootApp.sprigBootApp.exceptions.UserException;
import com.SprigBootApp.sprigBootApp.exceptions.UserExceptionCode;
import com.SprigBootApp.sprigBootApp.model.entity.User;
import com.SprigBootApp.sprigBootApp.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public Optional<User> getUser(int id) {
        return userRepository.findById(id);
    }


    public User createUser(String username, String password, String name) throws UserException {
        Optional<User> optionalUser = userRepository.findByUsername(username);
        if (optionalUser.isEmpty()){
            User user = new User();
            user.setUsername(username);
            user.setPassword(passwordEncoder.encode(password));
            user.setName(name);
            return userRepository.save(user);
        }
        throw new UserException(UserExceptionCode.USERNAME_ALREADY_EXIST);
    }

    @Override
    public Page<User> getAllUsers(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public boolean deleteUserById(int id){
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isEmpty()){
            return false;
        }
        userRepository.delete(optionalUser.get());
        return true;
    }

    public User updateUser(int id, String username, String password, String name) throws UserException {
        Optional<User> optionalUser = userRepository.findByUsername(username);
        if (optionalUser.isPresent()){
            if(optionalUser.get().getId() != id){
                throw new UserException(UserExceptionCode.USERNAME_ALREADY_EXIST);
            }
        }
        User user = new User();
        user.setId(id);
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));
        user.setName(name);
        return userRepository.save(user);
    }

}