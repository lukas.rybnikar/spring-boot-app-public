package com.SprigBootApp.sprigBootApp.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface DatabaseUserDetailsService extends UserDetailsService {
    public UserDetails loadUserByUsername(String username);
}
