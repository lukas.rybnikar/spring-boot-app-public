package com.SprigBootApp.sprigBootApp.service;

import com.SprigBootApp.sprigBootApp.exceptions.UserException;
import com.SprigBootApp.sprigBootApp.model.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface UserService {
    Optional<User> getUser(int id);
    boolean deleteUserById(int id);
    User updateUser(int id, String username, String password, String name) throws UserException;
    User createUser(String username, String password, String name) throws UserException;
    Page<User> getAllUsers(Pageable pageable);

}

