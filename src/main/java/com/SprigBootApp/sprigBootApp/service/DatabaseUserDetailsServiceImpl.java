package com.SprigBootApp.sprigBootApp.service;

import com.SprigBootApp.sprigBootApp.model.CustomUserDetails;
import com.SprigBootApp.sprigBootApp.model.entity.User;
import com.SprigBootApp.sprigBootApp.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DatabaseUserDetailsServiceImpl implements DatabaseUserDetailsService {
    private final
    UserRepository userRepository;

    public DatabaseUserDetailsServiceImpl(UserRepository userAccountRepository) {
        this.userRepository = userAccountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userAccount = userRepository.findByUsername(username);
        if(userAccount.isEmpty()){
            throw new UsernameNotFoundException("User with username [" + username + "] not found in the system");
        }
        return new CustomUserDetails(userAccount.get());
    }
}