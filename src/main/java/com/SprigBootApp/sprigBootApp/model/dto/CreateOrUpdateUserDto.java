package com.SprigBootApp.sprigBootApp.model.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class CreateOrUpdateUserDto {
    @NotBlank
    @Size(min = 1, max = 40, message = "Name should be {min} to  {max} character long")
    @Pattern(regexp = "[A-Za-z][A-Za-z\\s+]*[A-Za-z]")
    private String name;

    @NotBlank
    private String password;

    @NotBlank
    @Pattern(regexp = "[A-Za-z0-9]*")
    private String username;
}
