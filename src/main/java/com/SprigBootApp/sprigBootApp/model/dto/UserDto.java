package com.SprigBootApp.sprigBootApp.model.dto;

import com.SprigBootApp.sprigBootApp.model.entity.User;
import lombok.Data;

@Data
public class UserDto {

    // public ID would be better, but its currently not in DB
    private int id;

    private String name;

    private String username;

    public UserDto(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.username = user.getUsername();
    }
}
